'use strict';

/**
 * Adds commas to a number
 * @param {number} number
 * @param {string} locale
 * @return {string}
 */
module.exports = function(number,jsonParameters, locale) {

/*
    Las condiciones se concatenan al igual que los reduce
*/

    if(jsonParameters){
        console.log(jsonParameters);
        // Encontrar el Hash en IPFS

        // Verificar si esta en la BD 

        // Si esta en la BD, se recupera el hash de la solucion 
        //      se mezcla el valor recuperado, porque en la BD esta guardado en forma relativa
        //      y esta se regresada

        // Si no esta en la BD, 
        //      se simplifica
        //      Se Almacena el resultado en el IPFS
        //      y se Almacena en la BD #hashMatriz -> #hashResultado


/*
        Esto es lo que se guarda en la base de datos


        {"opr":"and&or","fexplicite":"11010110"}
            Esto se guarda en en IPFS y da un hash #434354645645645
        ==>

            Esto se guarda en el IPFS y da un hash #gfed23423453233
        {
            "labels":["#1","#2","#3"],
            "result":[
            {"fexplicite":"0100","condition":[],"reduce":["#2"]},
            {"fexplicite":"10","condition":[],"reduce":["#3","#1"]}
        ]}

            En la Base de datos se guarda
            #434354645645645 ==> #gfed23423453233

*/
        // Se retorna el resultado de la simplificación 
        return {
            "labels":["a","b","c"],
            "hash":"#9830239567844",
            "result":[
            {"fexplicite":"0100","condition":[],"reduce":["b"], "hash":"#fddase32343546"},
            {"fexplicite":"10","condition":[],"reduce":["c","a"], "hash":"#4534532452333"}
        ]};
    }

    return number.toLocaleString(locale);
};
